
Theme created by Ishmael Sanchez, www.ishmaelsanchez.com/earthish

General information
===================
* The theme is more for website that want to be more horizontally orientated.
* Theme uses absolute positioning for element placment (z-index for stacking)
* The large home page background image can be replaced at images/home-bg.jpg
* Similarly the content background image can updated at images/content-bg.jpg

Notes
=====
* Use the ie.css file for Internet Explorer (IE) hacks
	- Theme tested in IE 7 and 8
	- IE 6 renders but known issues with bottom bar region CSS
* styles.css file has the layout and the majority of the theme styles

TODO
====
Add option for admins to upload a custom background image
Refine CSS output in field.tpl.php
